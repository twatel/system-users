# system-users

This role has goal to install and configure unix users.  

It configure:
* Group's creation
    * Impose a GID
    * Impose a GNAME
    * Create group's before user creation
* User's creation
    * Imposea a UID
    * Impose a UNAME
    * Can import SSH Keys
    * Can configure SUDO access
    
## Setup development environment

### Setup environment 
```shell
sudo make install-python
sudo apt-get install direnv -y
if [ ! "$(grep -ir "direnv hook bash" ~/.bashrc)" ];then echo 'eval "$(direnv hook bash)"' >> ~/.bashrc; fi && direnv allow . && source ~/.bashrc
make env prepare
```

## Testing ansible-role-certbot execution in vagrant-docker environment

```shell
make test-docker
```

## Variables
By default the ``default/main.yml`` file is empty, that is for force to set everything outside of the role.

It exist two vars:
``system_groups_informations`` and ``system_users_informations``

### system_groups_informations
It variable store a list where each item is a dictionnary.
Each dictonnary must have two keys name
* **name** which is the group name
* **id** which is the group id

### system_users_informations
It variable store a list where each item is a dictionnary.
Each dictonnary must have nine keys name
* **name** store the user namme
* **id** store the user id
* **group_name** store the group name for it user
* **group_id** strore the group id of the user
* **groups** list of user additionals groups
* **home** the home directory path
* **sudo** ``true`` if the user have sudo capability
* **sudo_privileges** which sudo privileges to add
* **authorized_keys** a list of authorized_keys
* **shell** the shell of the user
* **groups** a list of group where the user have to be add

Here a exemple of a complete varaiabales file
``` yaml
---
system_groups_informations:
  - name: "docker"
    id: "7042"
  - name: "jenkins"
    id: "7043"
system_users_informations:
  - name: "test1"
    id: "42002"
    group_name: "group_for_test1"
    group_id: "42002"
    home: "/home/test1"
    sudo: false
    authorized_keys:
      - "ssh-rsa AAAùdflgmlkglmkdfgAB3NzaC1yc2EAA"
      - "ssh-rsa AAAAB3NzlmkjfslklmkaC1y2525c2EAA"
    shell: "/bin/bash"
    groups:
      - "docker"
      - "jenkins"
  - name: "test2"
    id: "42003"
    group_name: "group_for_test2"
    group_id: "42003"
    home: "/home/test2"
    sudo: true
    authorized_keys:
      - "ssh-rsa AAAùdflgmlkglmkdfgAB3NzaC1yc2EAA"
      - "ssh-rsa AAAAB3NzlmkjfslklmkaC1y2525c2EAA"
    shell: "/bin/bash"
    groups:
      - "docker"
      - "jenkins"
```

enjoy :)
